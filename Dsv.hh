<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\dsv
{
	use nuclio\plugin\format\driver\common\FormatException;
	use nuclio\plugin\format\driver\common\CommonInterface;

	use  nuclio\plugin\format\driver\dsv\handler\
	{
		Csv as CsvHandler,
		Dsv as DsvHandler,
		Tsv as TsvHandler
	};
	
	<<
	provides('format::dsv','format::csv','format::tsv'),
	singleton
	>>
	class Dsv implements CommonInterface
	{
		public function read(string $filename, Map<arraykey,mixed> $options=Map{}):Vector<Map<string,mixed>>
		{
			$extension		=pathinfo($filename,PATHINFO_EXTENSION);
			$data			=Vector{};
			$this->handle	=fopen($filename,'r');
			
			switch ($extension)
			{
				case 'dsv': $delimiter='-';		break;
				case 'tsv': $delimiter="\t";	break;
				case 'csv':
				default:
				{
					$delimiter=',';
				}
			}
			
			$mapHeaders=$options->get('mapHeaders');
			if ($mapHeaders)
			{
				$headers=fgetcsv
				(
					$this->handle,
					$options->get('length') ?? 0,
					$options->get('delimiter') ?? $delimiter,
					$options->get('enclosure') ?? '"',
					$options->get('escape_char') ?? '\\'
				);
				if ($headers===false)
				{
					return $data;
				}
			}
			while ($row=fgetcsv
				(
					$this->handle,
					$options->get('length') ?? 0,
					$options->get('delimiter') ?? $delimiter,
					$options->get('enclosure') ?? '"',
					$options->get('escape_char') ?? '\\'
				)
			)
			{
				if ($mapHeaders)
				{
					$data[]=new Map(array_combine($headers,$row));
				}
				else
				{
					$data[]=new Map($row);
				}
			}
			return $data;
		}
		
		public function write(string $filename, Vector<mixed> $data, Map<arraykey,mixed> $options=Map{}):bool
		{
			$extension		=pathinfo($filename,PATHINFO_EXTENSION);
			$handle			=fopen($filename, 'w');
			
			switch ($extension)
			{
				case 'dsv': $delimiter='-';		break;
				case 'tsv': $delimiter="\t";	break;
				case 'csv':
				default:
				{
					$delimiter=',';
				}
			}
			
			foreach ($data as $row)
			{
				fputcsv
				(
					$handle,
					$row,
					$options->get('delimiter') ?? $delimiter,
					$options->get('enclosure') ?? '"',
					$options->get('escape_char') ?? '\\'
				);
			}
			fclose($handle);
			return true;
		}
	}
}