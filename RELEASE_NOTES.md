Release Notes
-------------
2.0.0
-----
* [BC Break] Rewrote CSV driver. It is compatible with the new CommonInterface.

1.0.0
-----
* Initial Release.
